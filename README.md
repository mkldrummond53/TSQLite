# TSQLite

SQLite pur swift

![iOS](https://img.shields.io/badge/iOS-14.0.0-6193DF.svg)
![macOS](https://img.shields.io/badge/macOS-11.0.0-6193DF.svg)
![Xcode](https://img.shields.io/badge/Xcode-12.3-6193DF.svg)
![Swift Version](https://img.shields.io/badge/Swift-5.3-orange.svg) 
![SwiftPM Compatible](https://img.shields.io/badge/SwiftPM-compatible-brightgreen)
![Plaform](https://img.shields.io/badge/Platform-iOS-lightgrey.svg)
![Plaform](https://img.shields.io/badge/Platform-macOS-lightgrey.svg)
![License MIT](https://img.shields.io/badge/License-MIT-lightgrey.svg) 

