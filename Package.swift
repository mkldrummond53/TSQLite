// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TSQLite",
    platforms: [
        .macOS(.v11), .iOS(.v14)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "TSQLite",
            targets: ["TSQLite"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        //.package(path: "../TKit"),
        //.package(path: "../TCSQLite3"),
        //.package(path: "../TDataBase"),
        .package(url: "https://gitlab.com/tofprjs/shared/packages/TKit.git", .exact("1.1.1")),
        .package(url: "https://gitlab.com/tofprjs/macos-and-ios/packages/TCSQLite3.git", .exact("1.0.0")),
        .package(url: "https://gitlab.com/tofprjs/macos-and-ios/packages/TDataBase.git", .exact("1.1.5"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "TSQLite",
            dependencies: [
                .product(name: "TKit", package: "TKit"),
                .product(name: "TCSQLite3", package: "TCSQLite3"),
                .product(name: "TDataBase", package: "TDataBase"),
            ]
        ),
        .testTarget(
            name: "TSQLiteTests",
            dependencies: ["TSQLite"]),
    ]
)
