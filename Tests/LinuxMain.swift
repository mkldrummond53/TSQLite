import XCTest

import TSQLiteTests

var tests = [XCTestCaseEntry]()
tests += TSQLiteTests.allTests()
XCTMain(tests)
