import XCTest
import TDataBase
import TCSQLite3
@testable import TSQLite

final class TSQLiteTests: XCTestCase {
    
    func genData() -> [TSqliteDataRow] {
        var lRet:[TSqliteDataRow] = []
        
        let lDataSet = TSqliteDataSet(
            table: "customers",
            columns: ["CustomerId","FirstName","LastName","Company","Address","City","State","Country","Zip","Phone","Fax","Email","Date","Time"],
            types: [.int,.string,.string,.string,.string,.string,.string,.string,.string,.string,.string,.string,.date,.time]
        )

        for lI in 1...10 {
            let lRow = TSqliteDataRow(dataSet: lDataSet, values: [
                lI,
                "first name \(lI)" as Any,
                "last name \(lI)" as Any,
                "World Compagny \(lI)" as Any,
                "\(lI) Infinite Loop" as Any,
                "Cupertino" as Any,
                "CA" as Any,
                "USA" as Any,
                "\((9500 + lI - 1))" as Any,
                "91\(lI-1)" as Any,
                "81\(lI-1)" as Any,
                "john\(lI).doe@gmail.com" as Any,
                Date(),
                TimeInterval(lI*100)
            ])
            
            lRet.append(lRow)
        }
        
        return lRet
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        if FileManager.default.fileExists(atPath: lFile) {
            let _ = try? FileManager.default.removeItem(atPath: lFile)
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_createDb() {
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        let _ = TSqlite.open(path: lFile)
        
        XCTAssert(FileManager.default.fileExists(atPath: lFile))
    }
    
    func test_execCreateTable() {
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        do {
            if let lDb = TSqlite.open(path: lFile) {
                let lSql = TSql()
                    .create(.table, name: "customers")
                    .ifNotExist
                    .column(name: "CustomerId", type: .int, options: [.primary(.asc),.autoincrement])
                    .column(name: "FirstName", type: .string, options: [.not,.null])
                    .column(name: "LastName", type: .string, options: [.not,.null])
                    .column(name: "Company", type: .string)
                    .column(name: "Address", type: .string)
                    .column(name: "City", type: .string)
                    .column(name: "State", type: .string)
                    .column(name: "Country", type: .string)
                    .column(name: "Zip", type: .string)
                    .column(name: "Phone", type: .string)
                    .column(name: "Fax", type: .string)
                    .column(name: "Email", type: .string, options: [.not,.null])
                    .column(name: "Date", type: .date, options: [.not,.null])
                    .column(name: "Time", type: .time, options: [.not,.null])

                try lDb.exec(lSql)
            }
        } catch let lError {
            XCTFail("Error: \(lError)")
        }
    }
    
    func test_execInsertOneRecord() {
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        self.test_execCreateTable()
        
        do {
            if let lDb = TSqlite.open(path: lFile) {
                let lRow = self.genData()[0]
                let lColumns = ["FirstName","LastName","Company","Address","City","State","Country","Zip","Phone","Fax","Email","Date","Time"]
                let lSql = TSql()
                    .insertOrReplace("customers")
                    .columns(lColumns)
                    .values(lColumns.map({TSql.Value.var("\($0)")}))
                
                try lDb.exec(lSql, [lRow])

                XCTAssert(lDb.lastInsertRowId == 1)
            }
        } catch let lError {
            XCTFail("Error: \(lError)")
        }
    }
    
    func test_execInsertManyRecords() {
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        self.test_execCreateTable()
        
        do {
            if let lDb = TSqlite.open(path: lFile) {
                let lRows = self.genData()
                let lColumns = ["FirstName","LastName","Company","Address","City","State","Country","Zip","Phone","Fax","Email","Date","Time"]
                let lSql = TSql()
                    .insertOrReplace("customers")
                    .columns(lColumns)
                    .values(lColumns.map({TSql.Value.var("\($0)")}))
                
                try lDb.exec(lSql, lRows)

                XCTAssert(lDb.lastInsertRowId == 10)
                XCTAssert(lDb.lastRowId(table: "customers") == 10)
            }
        } catch let lError {
            XCTFail("Error: \(lError)")
        }
    }
    
    func test_querySelect() {
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        self.test_execInsertManyRecords()
        
        if let lDb = TSqlite.open(path: lFile) {
            let lRows = self.genData()
            
            XCTAssert(lDb.error == .success , "\(lDb.error)")
            
            let lSql = TSql()
                .select
                .all
                .from("customers")
            
            if let lResult = lDb.query(select: lSql, types: [.int,.string,.string,.string,.string,.string,.string,.string,.string,.string,.string,.string,.date,.time]) {
                XCTAssert(lResult.rows.count == lRows.count, "Received \(lResult.rows.count) must receive \(lRows.count)")
                
                XCTAssert(lResult.column(0) == "CustomerId", ".column(0) must equal to\"CustormerId\"")
                XCTAssert(lResult.column(1) == "FirstName", ".column(1) must equal to\"FirstName\"")
                XCTAssert(lResult.column(2) == "LastName", ".column(2) must equal to\"LastName\"")
                XCTAssert(lResult.column(3) == "Company", ".column(3) must equal to\"Company\"")
                XCTAssert(lResult.column(4) == "Address", ".column(4) must equal to\"Address\"")
                XCTAssert(lResult.column(5) == "City", ".column(5) must equal to\"City\"")
                XCTAssert(lResult.column(6) == "State", ".column(6) must equal to\"State\"")
                XCTAssert(lResult.column(7) == "Country", ".column(7) must equal to\"Country\"")
                XCTAssert(lResult.column(8) == "Zip", ".column(8) must equal to\"Zip\"")
                XCTAssert(lResult.column(9) == "Phone", ".column(9) must equal to\"Phone\"")
                XCTAssert(lResult.column(10) == "Fax", ".column(10) must equal to\"Fax\"")
                XCTAssert(lResult.column(11) == "Email", ".column(11) must equal to\"Email\"")
                XCTAssert(lResult.column(12) == "Date", ".column(12) must equal to\"Date\"")
                XCTAssert(lResult.column(13) == "Time", ".column(13) must equal to\"Time\"")

                if let lRow = lResult.row(0) {
                    let lCustomerId1:Int = lRow.cast(idx:0) ?? -1
                    let lCustomerId2:Int = lRow.cast(name: "CustomerId") ?? -1
                    XCTAssert(lCustomerId1 != -1)
                    XCTAssert(lCustomerId2 != -1)
                    XCTAssert(lCustomerId1 == lCustomerId2)
                    
                    let lFirstName1:String = lRow.cast(idx:1) ?? ""
                    let lFirstName2:String = lRow.cast(name:"FirstName") ?? ""
                    XCTAssert(lFirstName1 != "")
                    XCTAssert(lFirstName2 != "")
                    XCTAssert(lFirstName1 == lFirstName2)
                    
                    let lLastName1:String = lRow.cast(idx:2) ?? ""
                    let lLastName2:String = lRow.cast(name:"LastName") ?? ""
                    XCTAssert(lLastName1 != "")
                    XCTAssert(lLastName2 != "")
                    XCTAssert(lLastName1 == lLastName2)
                    
                    let lCompany1:String = lRow.cast(idx:3) ?? ""
                    let lCompany2:String = lRow.cast(name:"Company") ?? ""
                    XCTAssert(lCompany1 != "")
                    XCTAssert(lCompany2 != "")
                    XCTAssert(lCompany1 == lCompany2)
                    
                    let lAddress1:String = lRow.cast(idx:4) ?? ""
                    let lAddress2:String = lRow.cast(name:"Address") ?? ""
                    XCTAssert(lAddress1 != "")
                    XCTAssert(lAddress2 != "")
                    XCTAssert(lAddress1 == lAddress2)
                    
                    let lCity1:String = lRow.cast(idx:5) ?? ""
                    let lCity2:String = lRow.cast(name:"City") ?? ""
                    XCTAssert(lCity1 != "")
                    XCTAssert(lCity2 != "")
                    XCTAssert(lCity1 == lCity2)
                    
                    let lState1:String = lRow.cast(idx:6) ?? ""
                    let lState2:String = lRow.cast(name:"State") ?? ""
                    XCTAssert(lState1 != "")
                    XCTAssert(lState2 != "")
                    XCTAssert(lState1 == lState2)
                    
                    let lCountry1:String = lRow.cast(idx:7) ?? ""
                    let lCountry2:String = lRow.cast(name:"Country") ?? ""
                    XCTAssert(lCountry1 != "")
                    XCTAssert(lCountry2 != "")
                    XCTAssert(lCountry1 == lCountry2)
                    
                    let lZip1:String = lRow.cast(idx:8) ?? ""
                    let lZip2:String = lRow.cast(name:"Zip") ?? ""
                    XCTAssert(lZip1 != "")
                    XCTAssert(lZip2 != "")
                    XCTAssert(lZip1 == lZip2)
                    
                    let lPhone1:String = lRow.cast(idx:9) ?? ""
                    let lPhone2:String = lRow.cast(name:"Phone") ?? ""
                    XCTAssert(lPhone1 != "")
                    XCTAssert(lPhone2 != "")
                    XCTAssert(lPhone1 == lPhone2)
                    
                    let lFax1:String = lRow.cast(idx:10) ?? ""
                    let lFax2:String = lRow.cast(name:"Fax") ?? ""
                    XCTAssert(lFax1 != "")
                    XCTAssert(lFax2 != "")
                    XCTAssert(lFax1 == lFax2)
                    
                    let lEmail1:String = lRow.cast(idx:11) ?? ""
                    let lEmail2:String = lRow.cast(name:"Email") ?? ""
                    XCTAssert(lEmail1 != "")
                    XCTAssert(lEmail2 != "")
                    XCTAssert(lEmail1 == lEmail2)
                    
                    let lDate1:String = lRow.cast(idx:12) ?? ""
                    let lDate2:String = lRow.cast(name:"Date") ?? ""
                    XCTAssert(lDate1 != "")
                    XCTAssert(lDate2 != "")
                    XCTAssert(lDate1 == lDate2)
                    
                    let lTime1:String = lRow.cast(idx:13, sql: .time) ?? ""
                    let lTime2:String = lRow.cast(name:"Time", sql: .time) ?? ""
                    XCTAssert(lTime1 != "")
                    XCTAssert(lTime2 != "")
                    XCTAssert(lTime1 == lTime2)
                    
                }

            } else {
                XCTFail("Error: \(lDb.error)")
            }
        }
    }

    func json<T>(sql pSql:TSql) -> T? {
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        guard let lDb = TSqlite.open(path: lFile) else {
            return nil
        }
        
        defer {
            do {
                try lDb.close()
            } catch {
            }

            if FileManager.default.fileExists(atPath: lFile) {
                let _ = try? FileManager.default.removeItem(atPath: lFile)
            }
        }
        
        let lType = TSql.Field.any(type: T.self)
        
        guard lType != .unknown else {
            return nil
        }
        
        guard let lRet:T = lDb.query(select: pSql, types: [lType])?.cast() else {
            return nil
        }
        
        return lRet
    }
    
    func test_json() {
        var lSql = TSql()
            .select
            .json(#"{"key":1}"#)
        
        XCTAssert(json(sql: lSql) == #"{"key":1}"#)
        
        lSql = TSql()
            .select
            .json(["key":1])

        XCTAssert(json(sql: lSql) == #"{"key":1}"#)

        lSql = TSql()
            .select
            .json(["Value1","Value2","Value3"])

        XCTAssert(json(sql: lSql) == #"["Value1","Value2","Value3"]"#)
        
        lSql = TSql()
            .select
            .json(["value",99,3.14])
        
        XCTAssert(json(sql: lSql) == #"["value",99,3.14]"#)
    }

    func test_json_array() {
        var lSql = TSql()
            .select
            .json(.array([.i(1),.i(2),.s("3"),.i(4)]))
        
        XCTAssert(json(sql: lSql) == #"[1,2,"3",4]"#)

        lSql = TSql()
            .select
            .json(.array(["[1,2]".asValue]))
        
        XCTAssert(json(sql:lSql) == #"["[1,2]"]"#)

        lSql = TSql()
            .select
            .json(.array(["[1,2]".asValue]))
        
        XCTAssert(json(sql:lSql) == #"["[1,2]"]"#)

        lSql = TSql()
            .select
            .json(.array(.json(.array([1,2]))))

        XCTAssert(json(sql:lSql) == #"[[1,2]]"#)

        lSql = TSql()
            .select
            .json(.array([.i(1),.null,.s("3"),.s("[4,5]"),.s(#"{"six":7.7}"#)]))

        XCTAssert(json(sql:lSql) == #"[1,null,"3","[4,5]","{\"six\":7.7}"]"#)

        lSql = TSql()
            .select
            .json(.array([.i(1),.null,.s("3"),.json(.raw("[4,5]")),.json(.raw(#"{"six":7.7}"#))]))

        XCTAssert(json(sql:lSql) == #"[1,null,"3",[4,5],{"six":7.7}]"#)
    }

    func test_json_array_length() {
        var lSql = TSql()
            .select
            .json(.length([.s("[1,2,3,4]")]))

        XCTAssert(json(sql:lSql) == 4)

        lSql = TSql()
            .select
            .json(.length([.s("[1,2,3,4]"),.s("$")]))

        XCTAssert(json(sql:lSql) == 4)

        lSql = TSql()
            .select
            .json(.length([.s("[1,2,3,4]"),.s("$[2]")]))

        XCTAssert(json(sql:lSql) == 0)

        lSql = TSql()
            .select
            .json(.length([.s(#"{"one":[1,2,3]}"#)]))

        XCTAssert(json(sql:lSql) == 0)

        lSql = TSql()
            .select
            .json(.length([.s(#"{"one":[1,2,3]}"#),.s("$.one")]))

        XCTAssert(json(sql:lSql) == 3)

        lSql = TSql()
            .select
            .json(.length([.s(#"{"one":[1,2,3]}"#),.s("$.two")]))

        let lResult:Int? = json(sql: lSql)
        
        XCTAssert(lResult == nil)
    }

    func test_json_object() {
        var lSql = TSql()
            .select
            .json(.obj([.s("a"),.i(2),.s("c"),.i(4)]))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":4}"#)
        
        lSql = TSql()
            .select
            .json(.obj([.s("a"),.i(2),.s("c"),.s("{e:5}")]))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":"{e:5}"}"#)
        
        lSql = TSql()
            .select
            .json(.obj([.s("a"),.i(2),.s("c"),.json(.obj([.s("e"),.i(5)]))]))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":{"e":5}}"#)
    }
    
    func test_json_set() {
        var lSql = TSql()
            .select
            .json(.set(.s(#"{"a":2,"c":4}"#), #"$.a"#,99))

        XCTAssert(json(sql:lSql) == #"{"a":99,"c":4}"#)

        lSql = TSql()
            .select
            .json(.set(.s(#"{"a":2,"c":4}"#), #"$.c"#,.s(#"[97,96]"#)))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":"[97,96]"}"#)

        lSql = TSql()
            .select
            .json(.set(.s(#"{"a":2,"c":4}"#),#"$.c"#,.json(.raw(#"[97,96]"#))))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":[97,96]}"#)

        lSql = TSql()
            .select
            .json(.set(.s(#"{"a":2,"c":4}"#), #"$.c"#,.json(.array([97,96]))))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":[97,96]}"#)
    }
    
    func test_json_insert() {
        var lSql = TSql()
            .select
            .json(.insert(.s(#"{"a":2,"c":4}"#), #"$.a"#,99))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":4}"#)

        lSql = TSql()
            .select
            .json(.insert(.s(#"{"a":2,"c":4}"#), #"$.e"#,99))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":4,"e":99}"#)
    }
    
    func test_json_replace() {
        var lSql = TSql()
            .select
            .json(.replace(.s(#"{"a":2,"c":4}"#), #"$.a"#,99))

        XCTAssert(json(sql:lSql) == #"{"a":99,"c":4}"#)

        lSql = TSql()
            .select
            .json(.replace(.s(#"{"a":2,"c":4}"#), #"$.e"#,99))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":4}"#)
    }
    
    func test_json_extract() {
        var lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5,{"f":7}]}"#),["$"]))

        XCTAssert(json(sql:lSql) == #"{"a":2,"c":[4,5,{"f":7}]}"#)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5,{"f":7}]}"#),["$.c"]))

        XCTAssert(json(sql:lSql) == #"[4,5,{"f":7}]"#)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5,{"f":7}]}"#),["$.c[2]"]))

        XCTAssert(json(sql:lSql) == #"{"f":7}"#)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5,{"f":7}]}"#),["$.c[2].f"]))

        XCTAssert(json(sql:lSql) == 7)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5],"f":7}"#),["$.c","$.a"]))

        XCTAssert(json(sql:lSql) == #"[[4,5],2]"#)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5],"f":7}"#),["$.c[#-1]"]))

        XCTAssert(json(sql:lSql) == 5)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5,{"f":7}]}"#),["$.x"]))

        let lResult:Int? = json(sql: lSql)
        
        XCTAssert(lResult == nil)

        lSql = TSql()
            .select
            .json(.extract(.s(#"{"a":2,"c":[4,5,{"f":7}]}"#),["$.x","$.a"]))

        XCTAssert(json(sql:lSql) == #"[null,2]"#)
    }

    func test_json_patch() {
        var lSql = TSql()
            .select
            .json(.patch(.s(#"{"a":1,"b":2}"#),.s(#"{"c":3,"d":4}"#)))

        XCTAssert(json(sql:lSql) == #"{"a":1,"b":2,"c":3,"d":4}"#)

        lSql = TSql()
            .select
            .json(.patch(.s(#"{"a":[1,2],"b":2}"#),.s(#"{"a":9}"#)))

        XCTAssert(json(sql:lSql) == #"{"a":9,"b":2}"#)

        lSql = TSql()
            .select
            .json(.patch(.s(#"{"a":[1,2],"b":2}"#),.s(#"{"a":null}"#)))

        XCTAssert(json(sql:lSql) == #"{"b":2}"#)

        lSql = TSql()
            .select
            .json(.patch(.s(#"{"a":1,"b":2}"#),.s(#"{"a":9,"b":null,"c":8}"#)))

        XCTAssert(json(sql:lSql) == #"{"a":9,"c":8}"#)

        lSql = TSql()
            .select
            .json(.patch(.s(#"{"a":{"x":1,"y":2},"b":3}"#),.s(#"{"a":{"y":9},"c":8}"#)))

        XCTAssert(json(sql:lSql) == #"{"a":{"x":1,"y":9},"b":3,"c":8}"#)
    }
    
    func test_json_remove() {
        var lSql = TSql()
            .select
            .json(.remove(.s(#"[0,1,2,3,4]"#),["$[2]"]))

        XCTAssert(json(sql:lSql) == #"[0,1,3,4]"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"[0,1,2,3,4]"#),["$[2]","$[0]"]))

        XCTAssert(json(sql:lSql) == #"[1,3,4]"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"[0,1,2,3,4]"#),["$[2]","$[0]"]))

        XCTAssert(json(sql:lSql) == #"[1,3,4]"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"[0,1,2,3,4]"#),["$[0]","$[2]"]))

        XCTAssert(json(sql:lSql) == #"[1,2,4]"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"[0,1,2,3,4]"#),["$[#-1]","$[0]"]))

        XCTAssert(json(sql:lSql) == #"[1,2,3]"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"{"x":25,"y":42}"#)))

        XCTAssert(json(sql:lSql) == #"{"x":25,"y":42}"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"{"x":25,"y":42}"#),["$.z"]))

        XCTAssert(json(sql:lSql) == #"{"x":25,"y":42}"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"{"x":25,"y":42}"#),["$.y"]))

        XCTAssert(json(sql:lSql) == #"{"x":25}"#)

        lSql = TSql()
            .select
            .json(.remove(.s(#"{"x":25,"y":42}"#),["$"]))

        let lResult:String? = json(sql: lSql)
        
        XCTAssert(lResult == nil)
    }

    func test_json_type() {
        var lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#)))

        XCTAssert(json(sql:lSql) == #"object"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$"))

        XCTAssert(json(sql:lSql) == #"object"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a"))

        XCTAssert(json(sql:lSql) == #"array"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[0]"))

        XCTAssert(json(sql:lSql) == #"integer"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[1]"))

        XCTAssert(json(sql:lSql) == #"real"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[2]"))

        XCTAssert(json(sql:lSql) == #"true"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[3]"))

        XCTAssert(json(sql:lSql) == #"false"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[4]"))

        XCTAssert(json(sql:lSql) == #"null"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[5]"))

        XCTAssert(json(sql:lSql) == #"text"#)

        lSql = TSql()
            .select
            .json(.type(.s(#"{"a":[2,3.5,true,false,null,"x"]}"#),"$.a[6]"))

        let lResult:String? = json(sql: lSql)
        
        XCTAssert(lResult == nil)
    }
    
    func test_json_valid() {
        var lSql = TSql()
            .select
            .json(.valid(.s(#"{"x":35}"#)))

        XCTAssert(json(sql:lSql) == 1)

        lSql = TSql()
            .select
            .json(.valid(.s(#"{"x":35"#)))

        XCTAssert(json(sql:lSql) == 0)
    }
    
    func test_json_quote() {
        var lSql = TSql()
            .select
            .json(.quote(.d(3.14159, 5)))

        XCTAssert(json(sql:lSql) == "3.14159")

        lSql = TSql()
            .select
            .json(.quote(.s("verdant")))

        XCTAssert(json(sql:lSql) == #""verdant""#)
    }

    func jsonTable(_ pName:String) -> TSqlite? {
        var lRet:TSqlite? = nil
        let lFile = NSTemporaryDirectory() + "UnitTestSqlite.sqlite"
        
        do {
            if let lDb = TSqlite.open(path: lFile) {
                switch pName {
                case "users":
                    var lSql = TSql()
                        .create(.table, name: "users")
                        .ifNotExist
                        .column(name: "id", type: .int, options: [.primary(.asc),.not,.null])
                        .column(name: "name", type: .string, options: [.not,.null])
                        .column(name: "phone", type: .string)
                        .column(name: "email", type: .string, options: [.not,.null])
                        .column(name: "created", type: .date, options: [.not,.null])

                    try lDb.exec(lSql)
                    
                    let lColumns = ["id","name","phone","email","created"]
                    lSql = TSql()
                        .insertOrReplace("users")
                        .columns(lColumns)
                        .values(lColumns.map({TSql.Value.var("\($0)")}))
                    
                    let lDataSet = TSqliteDataSet(table: "users", columns: lColumns, types: [.int,.string,.string,.string,.date])

                    var lRows:[TSqliteDataRow] = []
                    var lRow = TSqliteDataRow(dataSet: lDataSet, values: [1, "Bob McFett" as Any, #"["514-123-256","704-536-697"]"# as Any, "bmcfett@hunters.com" as Any, "2017-01-01T15:30:20.123+01:00" as Any])
                    lRows.append(lRow)
                    
                    lRow = TSqliteDataRow(dataSet: lDataSet, values: [2, "Angus O'Vader" as Any, #"["514-554-236"]"# as Any, "angus.o@destroyers.com" as Any, "2017-03-01T15:30:20.123+01:00" as Any])
                    lRows.append(lRow)

                    lRow = TSqliteDataRow(dataSet: lDataSet, values: [3, "Imperator Colin" as Any, #"["514-123-256","703-467-998","530-899-236"]"# as Any, "c@c.c" as Any, "2017-04-01T15:30:20.123+01:00" as Any])
                    lRows.append(lRow)
                    
                    try lDb.exec(lSql, lRows)
                case "big":
                    var lSql = TSql()
                        .create(.table, name: "big")
                        .ifNotExist
                        .column(name: "json", type: .string, options: [.not,.null])

                    try lDb.exec(lSql)
                    
                    let lColumns = ["json"]
                    lSql = TSql()
                        .insertOrReplace("big")
                        .columns(lColumns)
                        .values(lColumns.map({TSql.Value.var("\($0)")}))
                    
                    let lDataSet = TSqliteDataSet(table: "big", columns: lColumns,types: [.string])

                    var lRows:[TSqliteDataRow] = []
                    var lRow = TSqliteDataRow(dataSet: lDataSet, values: [
                        """
                         {
                            "id":123,
                            "stuff":[1,2,3,4],
                            "partlist":[
                               {"uuid":"bb108722-572e-11e5-9320-7f3b63a4ca74"},
                               {"uuid":"c690dc14-572e-11e5-95f9-dfc8861fd535"},
                               {"subassembly":[
                                  {"uuid":"6fa5181e-5721-11e5-a04e-57f3d7b32808"}
                               ]}
                            ]
                          }
                        """ as Any])
                    lRows.append(lRow)
                    
                    lRow = TSqliteDataRow(dataSet: lDataSet, values: [
                        """
                          {
                            "id":456,
                            "stuff":["hello","world","xyzzy"],
                            "partlist":[
                               {"uuid":false},
                               {"uuid":"c690dc14-572e-11e5-95f9-dfc8861fd535"}
                            ]
                          }
                        """ as Any])
                    lRows.append(lRow)
                    
                    try lDb.exec(lSql, lRows)
                default:
                    break
                }
                
                lRet = lDb
            }
        } catch let lError {
            XCTFail("Error: \(lError)")
        }
        
        return lRet
    }

    func test_json_group_object() {
        guard let lDb = jsonTable("users") else {
            XCTFail()
            return
        }
        
        var lSql = TSql()
            .select
            .all
            .from("users")
        
        guard let lSelectAll = lDb.query(select: lSql, types:[.int,.string,.string,.string,.date]) else {
            XCTFail()
            return
        }
        
        XCTAssert(lSelectAll.rows.count == 3)
        
        lSql = TSql()
            .select
            .as("json_result",
                .json(.group(.obj("email",.json(.obj([.s("name"),.r("name"),.s("created"),.r("created")])))))
            )
            .from(TSql
                    .select
                    .all
                    .from("users")
                    .where(.gt("created",.s("02-01-01")))
            )

        guard let lDataSet = lDb.query(select: lSql, types: [.string]) else {
            XCTFail()
            return
        }

        guard let lResult:String = lDataSet.cast() else {
            XCTFail()
            return
        }
        
        XCTAssert(lResult == #"{"bmcfett@hunters.com":{"name":"Bob McFett","created":"2017-01-01T15:30:20.123+01:00"},"angus.o@destroyers.com":{"name":"Angus O'Vader","created":"2017-03-01T15:30:20.123+01:00"},"c@c.c":{"name":"Imperator Colin","created":"2017-04-01T16:30:20.123+02:00"}}"#)
    }

    func test_json_group_array() {
        guard let lDb = jsonTable("users") else {
            XCTFail()
            return
        }
        
        var lSql = TSql()
            .select
            .all
            .from("users")
        
        guard let lSelectAll = lDb.query(select: lSql, types: [.int,.string,.string,.string,.date]) else {
            XCTFail()
            return
        }
        
        XCTAssert(lSelectAll.rows.count == 3)
        
        lSql = TSql()
            .select
            .as(.json(.group(.array(.json(.obj([.s("name"),.r("name"),.s("created"),.r("created")]))))))
            .from(TSql
                    .select
                    .all
                    .from("users")
                    .orderBy([.column("created")])
            )

        guard let lDataSet = lDb.query(select: lSql, types: [.string]) else {
            XCTFail()
            return
        }

        guard let lResult:String = lDataSet.cast() else {
            XCTFail()
            return
        }
        
        XCTAssert(lResult == #"[{"name":"Bob McFett","created":"2017-01-01T15:30:20.123+01:00"},{"name":"Angus O'Vader","created":"2017-03-01T15:30:20.123+01:00"},{"name":"Imperator Colin","created":"2017-04-01T16:30:20.123+02:00"}]"#)
    }

    func test_json_each() {
        guard let lDb = jsonTable("users") else {
            XCTFail()
            return
        }
        
        var lSql = TSql()
            .select
            .all
            .from("users")
        
        guard let lSelectAll = lDb.query(select: lSql, types: [.int,.string,.string,.string,.date]) else {
            XCTFail()
            return
        }
        
        XCTAssert(lSelectAll.rows.count == 3)
        
        lSql = TSql()
            .select
            .column("users.name")
            .from(TSql
                    .table("users")
                    .json(.each(.field("users.phone")))
            )
            .where(
                .and([
                    .json(.valid("users.phone")),
                    .field(.json(.each(.value)),.like("530-%"))
                ])
            )

        let lResult:String = lDb.query(select: lSql, types: [.string])?.cast() ?? ""

        XCTAssert(lResult == #"Imperator Colin"#)
    }

    func test_json_tree() {
        guard let lDb = jsonTable("big") else {
            XCTFail()
            return
        }
        
        var lSql = TSql()
            .select
            .all
            .from("big")
        
        guard let lSelectAll = lDb.query(select: lSql, types: [.string]) else {
            XCTFail()
            return
        }
        
        XCTAssert(lSelectAll.rows.count == 2)
        
        lSql = TSql()
            .select
            .as(.json(.group(.array(.json(.obj([
                .s("big.rowid"),.r("big.rowid"),
                .s("fullkey"),.r("fullkey"),
                .s("value"),.r("value")
            ]))))))
            .from(
                TSql
                    .table("big")
                    .json(.tree(.field("big.json")))
            )
            .where(
                .field(
                    .json(.tree(.type)),
                    .not(
                        .in([
                            "object".asValue,
                            "array".asValue
                        ])
                    )
                )
            )

        var lResult:String = lDb.query(select: lSql, types: [.string])?.cast() ?? ""

        XCTAssert(lResult == #"[{"big.rowid":1,"fullkey":"$.id","value":123},{"big.rowid":1,"fullkey":"$.stuff[0]","value":1},{"big.rowid":1,"fullkey":"$.stuff[1]","value":2},{"big.rowid":1,"fullkey":"$.stuff[2]","value":3},{"big.rowid":1,"fullkey":"$.stuff[3]","value":4},{"big.rowid":1,"fullkey":"$.partlist[0].uuid","value":"bb108722-572e-11e5-9320-7f3b63a4ca74"},{"big.rowid":1,"fullkey":"$.partlist[1].uuid","value":"c690dc14-572e-11e5-95f9-dfc8861fd535"},{"big.rowid":1,"fullkey":"$.partlist[2].subassembly[0].uuid","value":"6fa5181e-5721-11e5-a04e-57f3d7b32808"},{"big.rowid":2,"fullkey":"$.id","value":456},{"big.rowid":2,"fullkey":"$.stuff[0]","value":"hello"},{"big.rowid":2,"fullkey":"$.stuff[1]","value":"world"},{"big.rowid":2,"fullkey":"$.stuff[2]","value":"xyzzy"},{"big.rowid":2,"fullkey":"$.partlist[0].uuid","value":0},{"big.rowid":2,"fullkey":"$.partlist[1].uuid","value":"c690dc14-572e-11e5-95f9-dfc8861fd535"}]"#)

        lSql = TSql()
            .select
            .as(.json(.group(.array(.json(.obj([
                .s("big.rowid"),.r("big.rowid"),
                .s("fullkey"),.r("fullkey"),
                .s("value"),.r("value")
            ]))))))
            .from(
                TSql
                    .table("big")
                    .json(.tree(.field("big.json")))
            )
            .where(.field("atom",.not(.isNull)))

        lResult = lDb.query(select: lSql, types: [.string])?.cast() ?? ""

        XCTAssert(lResult == #"[{"big.rowid":1,"fullkey":"$.id","value":123},{"big.rowid":1,"fullkey":"$.stuff[0]","value":1},{"big.rowid":1,"fullkey":"$.stuff[1]","value":2},{"big.rowid":1,"fullkey":"$.stuff[2]","value":3},{"big.rowid":1,"fullkey":"$.stuff[3]","value":4},{"big.rowid":1,"fullkey":"$.partlist[0].uuid","value":"bb108722-572e-11e5-9320-7f3b63a4ca74"},{"big.rowid":1,"fullkey":"$.partlist[1].uuid","value":"c690dc14-572e-11e5-95f9-dfc8861fd535"},{"big.rowid":1,"fullkey":"$.partlist[2].subassembly[0].uuid","value":"6fa5181e-5721-11e5-a04e-57f3d7b32808"},{"big.rowid":2,"fullkey":"$.id","value":456},{"big.rowid":2,"fullkey":"$.stuff[0]","value":"hello"},{"big.rowid":2,"fullkey":"$.stuff[1]","value":"world"},{"big.rowid":2,"fullkey":"$.stuff[2]","value":"xyzzy"},{"big.rowid":2,"fullkey":"$.partlist[0].uuid","value":0},{"big.rowid":2,"fullkey":"$.partlist[1].uuid","value":"c690dc14-572e-11e5-95f9-dfc8861fd535"}]"#)

        lSql = TSql()
            .select
            .distinct
            .as(.json(.extract("big.json", ["$.id"])))
            .from(
                TSql
                    .table("big")
                    .json(.tree(.field("big.json","$.partlist")))
            )
            .where(
                .and([
                    .eq(.json(.tree(.key)),"uuid".asValue),
                    .eq(.json(.tree(.value)),"6fa5181e-5721-11e5-a04e-57f3d7b32808".asValue)
                ])
            )

        let lId:Int = lDb.query(select: lSql, types: [.int])?.cast() ?? 0

        XCTAssert(lId == 123)
    }

    static var allTests = [
        ("test_createDb", test_createDb),
        ("test_execCreateTable", test_execCreateTable),
        ("test_execInsertOneRecord", test_execInsertOneRecord),
        ("test_execInsertManyRecords", test_execInsertManyRecords),
        ("test_querySelect", test_querySelect),
        ("test_json", test_json),
        ("test_json_array", test_json_array),
        ("test_json_array_length", test_json_array_length),
        ("test_json_object", test_json_object),
        ("test_json_set", test_json_set),
        ("test_json_insert", test_json_insert),
        ("test_json_replace", test_json_replace),
        ("test_json_extract", test_json_extract),
        ("test_json_patch", test_json_patch),
        ("test_json_remove", test_json_remove),
        ("test_json_type", test_json_type),
        ("test_json_valid", test_json_valid),
        ("test_json_quote", test_json_quote),
        ("test_json_group_object", test_json_group_object),
        ("test_json_group_array", test_json_group_array),
        ("test_json_each", test_json_each),
        ("test_json_tree", test_json_tree),
    ]
    
}
