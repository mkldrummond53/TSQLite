//
//  TSqlite.swift
//  TSqlite
//
//  Created by Christophe Braud on 20/08/2017.
//  Base on Tof Templates (https://bit.ly/2Zk06Yg)
//  Copyright © 2017 Christophe Braud. All rights reserved.
//

import Foundation
import TKit
import TDataBase
import SQLite3

// Nothing to override or redefine because TSql support SQLite natively
public final class TSqliteBuilder : TSql {}

// MARK: -
// MARK: TSqlite
// MARK: -
public final class TSqlite: TDataBaseProperties {
    // MARK: -
    // MARK: Public access
    // MARK: -
    
    // MARK: -> Public enums
    
    // MARK: -> Public structs
    
    // MARK: -> Public class
    
    // MARK: -> Public type alias
    
    // MARK: -> Public static properties
    
    // MARK: -> Public properties
    
    public var isOpen: Bool = false
    public var error:TDbRet = .notInit
    
    private(set) public var path:String = ""
    
    public var row:TDataRow.Type {
        return TSqliteDataRow.self
    }
    
    public var dataSet:TDataSet.Type {
        return TSqliteDataSet.self
    }
    
    public var sqlBuilder: TSql.Type {
        return TSqliteBuilder.self
    }
    
    // MARK: -> Public class methods
    
    public static func appInstance(folder pFolder:String? = nil) -> TSqlite {
        var lRet:TSqlite
        
        if let lDb = appDb {
            lRet = lDb
        } else {
            #if os(macOS)
            let lDefaultFolder = TFolder.resourcesDirectory
            #else
            let lDefaultFolder = TFolder.documentDirectory
            #endif
            
            if var lFolder = pFolder ?? lDefaultFolder {
                if !lFolder.hasSuffix("/") {
                    lFolder += "/"
                }
                
                let lFile = TVersion.App.name + ".sqlite"
                
                if let lDb = open(path: lFolder + lFile) {
                    appDb = lDb
                    lRet = lDb
                } else {
                    // error set to '.notInit'
                    lRet = TSqlite()
                }
            } else {
                // error set to '.notInit'
                lRet = TSqlite()
            }
        }
        
        return lRet
    }

    public static func open(path pPath:String) -> TSqlite? {
        var lRet: TSqlite? = nil
        let lSqlite = TSqlite(pPath)
        
        if lSqlite.error == .success {
            lRet = lSqlite
        }
        
        return lRet
    }
    
    // MARK: -> Public init methods
    
    public convenience init() {
        self.init(":memory:")
    }
    
    public required init(_ pPath:String) {
        path = pPath
        reopen()
    }
    
    deinit {
        try? close()
    }
    
    // MARK: -> Public operators
    
    // MARK: -> Public methods
    
    // MARK: -
    // MARK: Internal access (aka public for current module)
    // MARK: -
    
    // MARK: -> Internal enums
    
    // MARK: -> Internal structs
    
    // MARK: -> Internal class
    
    // MARK: -> Internal type alias
    
    // MARK: -> Internal static properties
    
    // MARK: -> Internal properties
    
    // MARK: -> Internal class methods
    
    // MARK: -> Internal init methods
    
    // MARK: -> Internal operators
    
    // MARK: -> Internal methods
    
    // MARK: -> Internal implementation protocol <#protocol name#>
    
    // MARK: -
    // MARK: File Private access
    // MARK: -
    
    // MARK: -> File Private enums
    
    // MARK: -> File Private structs
    
    // MARK: -> File Private class
    
    // MARK: -> File Private type alias
    
    // MARK: -> File Private static properties
    
    // MARK: -> File Private properties
    
    // MARK: -> File Private class methods
    
    // MARK: -> File Private init methods
    
    // MARK: -> File Private operators
    
    // MARK: -> File Private methods
    
    // MARK: -
    // MARK: Private access
    // MARK: -
    
    // MARK: -> Private enums
    
    // MARK: -> Private structs
    
    // MARK: -> Private class
    
    // MARK: -> Private type alias
    
    private typealias Handler = () -> Int32
    private typealias Failure = (Int32) -> Void
    private typealias Success = () -> Void
    
    // MARK: -> Private static properties
    
    private static var appDb:TSqlite? = nil
    
    // MARK: -> Private properties
    
    private var db:OpaquePointer? = nil
    private var transactions:Int = 0
    
    // MARK: -> Private class methods
    
    // MARK: -> Private init methods
    
    // MARK: -> Private operators
    
    // MARK: -> Private methods
    
    private func error(_ pError:Int32) -> String? {
        return String(cString: sqlite3_errstr(pError))
    }
    
    private func sqliteAPI(_ pCode:Int32? = nil, valid pValid:[Int32] = [SQLITE_OK], ignores pIgnores:[Int32] = [], handle pHandle:OpaquePointer? = nil, loop pLoop:Bool = false, handler pHandler:Handler? = nil, failure pFailure:Failure = {pCode in}, success pSuccess:Success = {}) {
        let lHandle = pHandle ?? db
        
        repeat {
            let lCode:Int32 = pCode ?? (pHandler != nil ? pHandler!() : SQLITE_OK)
            
            if pValid.contains(lCode) {
                pSuccess()
            } else {
                if !pIgnores.contains(lCode) {
                    error = .error(code: lCode, message: String(cString: sqlite3_errmsg(lHandle)))
                }
                pFailure(lCode)
                break
            }
        } while pLoop
    }
    
    private func bind(_ pStatement:OpaquePointer, name pName:String, type pType:TSql.Field, value pValue:Any?) -> Int32 {
        let lColumn = sqlite3_bind_parameter_index(pStatement, ":" + pName)
        let lRet = bind(pStatement, column: lColumn, type: pType, value: pValue)
        return lRet
    }
    
    private func bind(_ pStatement:OpaquePointer, column pColumn:Int32, type pType:TSql.Field, value pValue:Any?) -> Int32 {
        var lRet:Int32 = SQLITE_ERROR
        
        if pColumn > 0 {
            if pValue == nil {
                lRet = sqlite3_bind_null(pStatement, pColumn)
            } else {
                switch pType {
                case .unknown, .any, .null:
                    lRet = sqlite3_bind_null(pStatement, pColumn)
                case .bool:
                    if let lValue = pValue as? Bool {
                        lRet = sqlite3_bind_int(pStatement, pColumn, Int32(lValue ? 1 : 0))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .int:
                    if let lValue = pValue as? Int {
                        lRet = sqlite3_bind_int(pStatement, pColumn, Int32(lValue))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .int32:
                    if let lValue = pValue as? Int32 {
                        lRet = sqlite3_bind_int(pStatement, pColumn, lValue)
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .int64:
                    if let lValue = pValue as? Int64 {
                        lRet = sqlite3_bind_int64(pStatement, pColumn, lValue)
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .float:
                    if let lValue = pValue as? Float {
                        lRet = sqlite3_bind_double(pStatement, pColumn, Double(lValue))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .double:
                    if let lValue = pValue as? Double {
                        lRet = sqlite3_bind_double(pStatement, pColumn, Double(lValue))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .string:
                    if let lValue = pValue as? String {
                        lRet = sqlite3_bind_text(pStatement, pColumn, lValue, -1, unsafeBitCast(-1, to: sqlite3_destructor_type.self))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .time:
                    if let lValue = pValue as? String {
                        lRet = sqlite3_bind_text(pStatement, pColumn, lValue, -1, unsafeBitCast(-1, to: sqlite3_destructor_type.self))
                    } else if let lValue = pValue as? TimeInterval {
                        let lString = TData.string(from: lValue)
                        lRet = sqlite3_bind_text(pStatement, pColumn, lString, -1, unsafeBitCast(-1, to: sqlite3_destructor_type.self))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .date:
                    if let lValue = pValue as? String {
                        lRet = sqlite3_bind_text(pStatement, pColumn, lValue, -1, unsafeBitCast(-1, to: sqlite3_destructor_type.self))
                    } else if let lValue = pValue as? Date {
                        let lString = TData.string(from: lValue)
                        lRet = sqlite3_bind_text(pStatement, pColumn, lString, -1, unsafeBitCast(-1, to: sqlite3_destructor_type.self))
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                case .data:
                    if let lValue = pValue as? Data {
                        let _ = lValue.withUnsafeBytes {
                            pBytes in
                            lRet = sqlite3_bind_blob(pStatement, pColumn, pBytes.baseAddress, Int32(lValue.count), unsafeBitCast(-1, to: sqlite3_destructor_type.self))
                        }
                    } else {
                        lRet = sqlite3_bind_null(pStatement, pColumn)
                    }
                }
            }
        }
        
        return lRet
    }
    
    private func value(_ pStatement:OpaquePointer, type pType:TSql.Field, idx pIdx:Int32) -> Any? {
        var lRet:Any? = nil
        
        if pIdx >= 0 {
            switch sqlite3_column_type(pStatement, pIdx) {
            case SQLITE_NULL:
                break
            case SQLITE_INTEGER:
                let lSqliteVal = sqlite3_column_int64(pStatement, pIdx)
                switch pType {
                case .any:
                    lRet = lSqliteVal
                case .bool:
                    lRet = Bool(lSqliteVal == 1 ? true : false)
                case .int:
                    lRet = Int(lSqliteVal)
                case .int32:
                    lRet = Int32(lSqliteVal)
                case .int64:
                    lRet = Int64(lSqliteVal)
                case .time:
                    lRet = TimeInterval(lSqliteVal)
                case .date:
                    let lTimeInterval = TimeInterval(lSqliteVal)
                    lRet = Date(timeIntervalSince1970: lTimeInterval)
                default:
                    lRet = nil
                }
            case SQLITE_FLOAT:
                let lSqliteVal = sqlite3_column_double(pStatement, pIdx)
                switch pType {
                case .any:
                    lRet = lSqliteVal
                case .float:
                    lRet = Float(lSqliteVal)
                case .double:
                    lRet = Double(lSqliteVal)
                case .time:
                    lRet = TimeInterval(lSqliteVal)
                case .date:
                    let lTimeInterval = TimeInterval(lSqliteVal)
                    lRet = Date(timeIntervalSince1970: lTimeInterval)
                default:
                    lRet = nil
                }
            case SQLITE_TEXT:
                let lString = String(cString: UnsafePointer(sqlite3_column_text(pStatement, pIdx)))
                
                switch pType {
                case .unknown, .null:
                    lRet = nil
                case .any:
                    lRet = lString
                case .bool:
                    lRet = Int(lString) == 1
                case .int:
                    lRet = Int(lString)
                case .int32:
                    lRet = Int32(lString)
                case .int64:
                    lRet = Int64(lString)
                case .float:
                    lRet = Float(lString)
                case .double:
                    lRet = Double(lString)
                case .string:
                    lRet = lString
                case .time:
                    lRet = TData.timeInterval(from: lString)
                case .date:
                    lRet = TData.date(from: lString)
                case .data:
                    lRet = Data(base64Encoded: lString)
                }
            case SQLITE_BLOB:
                if let lPointer = sqlite3_column_blob(pStatement, pIdx) {
                    let lCount = sqlite3_column_bytes(pStatement, pIdx)
                    let lData = Data(bytes:lPointer, count: Int(lCount))
                    lRet = lData
                }
            default:
                break
            }
        }
        
        return lRet
    }
}

// MARK: -
// MARK: Interface implementation protocol TDataBaseRaw
// MARK: -
extension TSqlite: TDataBaseBasic {
    public var lastInsertRowId: Int64 {
        guard let lDb = db else {
            return -1
        }

        return sqlite3_last_insert_rowid(lDb)
    }
    
    
    public func close() throws {
        error = .success
        
        guard let lDb = db else {
            error = .missing(.database)
            throw error
        }
        
        sqliteAPI(sqlite3_close(lDb), success:  {
            db = nil
            isOpen = false
        })
    }
    
    public func reopen() {
        do {
            if isOpen {try close()}
            
            var lDb:OpaquePointer? = nil
            let lPath = path.isEmpty == false ? path : ":memory:"
            let lFlags = SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX
            
            sqliteAPI(sqlite3_open_v2(lPath, &lDb, lFlags, nil), handle: lDb,
                      failure: {
                        (pError:Int32) in
                        error = .error(code: pError, message: error(pError) ?? "unknown error")
                      },
                      success:  {
                        db = lDb
                        isOpen = true
                        error = .success
                      })
        } catch {
        }
    }
    
    public func exec(_ pSql: TSql) throws {
        error = .success
        
        guard let lDb = db else {
            error = .missing(.database)
            throw error
        }
        
        sqliteAPI(sqlite3_exec(lDb, pSql.build(), nil, nil, nil))
    }
    
    public func exec(_ pSql: TSql, _ pRows: [TDataRow]) throws {
        error = .success
        
        guard let lDb = db else {
            error = .missing(.database)
            throw error
        }
        
        var lPrepare: OpaquePointer? = nil
        
        sqliteAPI(sqlite3_prepare_v2(lDb, pSql.build(), -1, &lPrepare, nil), success:  {
            if let lStatement = lPrepare {
                let lCount = sqlite3_bind_parameter_count(lStatement)
                
                if lCount == 0 {
                    try? exec(pSql)
                } else {
                    let lNames = (1...lCount).compactMap({
                        pColumn -> String? in
                        
                        if let lRawName = sqlite3_bind_parameter_name(lStatement, pColumn) {
                            return String(String(cString:lRawName).dropFirst())
                        }
                        return nil
                    })
                    
                    if lNames.count != lCount {
                        error = .error(code: Int32(sqlite3_sql(lStatement).pointee), message: String(cString: sqlite3_errmsg(lStatement)))
                    } else {
                        var lContinue = true
                        
                        for lI1 in 0..<pRows.count where lContinue == true {
                            let lRow = pRows[lI1]
                            
                            for lI2 in 0..<lNames.count where lContinue == true {
                                let lName = lNames[lI2]
                                let lType = lRow.type(name: lName)
                                
                                if let lAnyType = lType.asAnyType, lType != .unknown {
                                    let lValue = lRow.cast(name: lName, type: lAnyType, sql: lType)

                                    sqliteAPI(bind(lStatement, name: lName, type: lType, value: lValue), handle: lStatement, failure: {
                                        pCode in
                                        lContinue = false
                                        error = .error(code: pCode, message: error(pCode) ?? "unknown error")
                                    })
                                } else {
                                    lContinue = false
                                    error = .missing(.type)
                                }
                            }
                            
                            if lContinue == true {
                                sqliteAPI(sqlite3_step(lStatement), valid: [SQLITE_DONE], handle: lStatement,
                                          failure: {
                                            pCode in
                                            lContinue = false
                                            error = .error(code: pCode, message: error(pCode) ?? "unknown error")
                                          },
                                          success: {
                                            sqliteAPI(sqlite3_reset(lStatement), handle: lStatement, failure: {
                                                pCode in
                                                lContinue = false
                                                error = .error(code: pCode, message: error(pCode) ?? "unknown error")
                                            })
                                          })
                            }
                        }
                        sqliteAPI(sqlite3_finalize(lStatement), handle: lStatement)
                    }
                }
            }
        })
        
        if error != .success {
            throw error
        }
    }
    
    public func query(select pSelect: TSql, columns pColumns:[String] = [], types pTypes:[TSql.Field]) -> TDataSet? {
        var lRet:TDataSet? = nil
        var lRow:TSqliteDataRow? = nil
        
        query(select: pSelect, columns: pColumns, types: pTypes) {
            pState in
            
            switch pState {
            case .columns(let lColumns):
                lRet = TSqliteDataSet(columns: lColumns, types: pTypes)
            case .begin:
                if let lDataSet = lRet {
                    lRow = TSqliteDataRow(dataSet: lDataSet)
                }
            case .value(let lIdx, _, let lValue):
                lRow?.set(idx: lIdx, value: lValue)
            case .end:
                if var lDataSet = lRet, let lRow = lRow {
                    lDataSet.rows.append(lRow)
                }
            case .close:
                break
            case .error(_):
                lRet = nil
            }
        }
        
        return lRet
    }
    
    public func query(select pSelect: TSql, columns pColumns:[String] = [], types pTypes:[TSql.Field], handler pHandler:@escaping SelectHandler) {
        error = .success
        
        guard let lDb = db else {
            error = .missing(.database)
            pHandler(.error(error))
            return
        }
        
        var lPrepare: OpaquePointer? = nil
        var lColumns:[String] = pColumns
        
        if lColumns.count > 0 {
            pHandler(.columns(lColumns))
        }
        
        sqliteAPI(sqlite3_prepare_v2(lDb, pSelect.build(), -1, &lPrepare, nil), success:  {
            if let lStatement = lPrepare {
                
                sqliteAPI(valid: [SQLITE_ROW], ignores: [SQLITE_DONE], handle: lStatement, loop: true,
                          handler:{
                            return sqlite3_step(lStatement)
                          },
                          failure: {
                            pCode in
                            if pCode == SQLITE_DONE {
                                error = .success
                            }
                            
                            sqliteAPI(sqlite3_finalize(lStatement), handle: lStatement)
                            
                            if error == .success {
                                pHandler(.close)
                            } else {
                                pHandler(.error(error))
                            }
                          },
                          success: {
                            var lCount = lColumns.count
                            
                            if lCount == 0 {
                                lCount = Int(sqlite3_column_count(lStatement))
                                lColumns = (0..<Int32(lCount)).map({String(cString: sqlite3_column_name(lStatement, $0))})
                                pHandler(.columns(lColumns))
                            }
                            
                            if lCount > 0 {
                                pHandler(.begin)
                                
                                for lI in 0..<lCount {
                                    let lColumn = lColumns[lI]
                                    if let lType = pTypes[safeIndex:lI] {
                                        let lValue = value(lStatement, type: lType, idx: Int32(lI))
                                        pHandler(.value(lI, lColumn, lValue))
                                    } else {
                                        pHandler(.error(.missing(.type)))
                                    }
                                }
                                
                                pHandler(.end)
                            }
                          })
            }
        })
    }
    
    public func transaction(_ pTransaction: TDbTransaction) {
        switch pTransaction {
        case .begin:
            do {
                try exec(TSqliteBuilder().transaction(.begin))
                transactions += 1
            } catch {
            }
        case .rollback:
            if transactions > 0 {
                do {
                    try exec(TSqliteBuilder().transaction(.rollback))
                    transactions -= 1
                } catch {
                }
            }
        case .commit:
            if transactions > 0 {
                do {
                    try exec(TSqliteBuilder().transaction(.commit))
                    transactions -= 1
                } catch {
                }
            }
        }
    }
}

// MARK: -
// MARK: Interface implementation protocol TDataBaseAdvanced
// MARK: -
extension TSqlite: TDataBaseAdvanced {
    public func exist(table pTable:String) -> Bool {
        let lSql = TSqliteBuilder()
            .select
            .column("name")
            .from("sqlite_master")
            .where(.and([.eq("type",.s("table")),.eq("name",.s(pTable))]))
        
        let lDataSet = query(select: lSql,types: [.string])
        return lDataSet?.rows.count == 1
    }
    
    public func lastRowId(table pTable:String) -> Int64 {
        var lRet:Int64 = 0
        let lSql = TSqliteBuilder()
            .select
            .max("rowid")
            .from(pTable)
        
        if let lLastRowId:Int64 = self.query(select: lSql, types: [.int64])?.cast() {
            lRet = lLastRowId
        }
        
        return lRet
    }
    
    public func count(table pTable:String) -> Int {
        let lRet = count(table: pTable, nil)
        return lRet
    }
    
    public func count(table pTable:String, _ pSql:TSql?) -> Int {
        var lRet:Int = 0
        let lSql = TSqliteBuilder()
            .select
            .count(.all)
            .from(pTable)
        
        if let lAppend = pSql {
            lSql.append(lAppend)
        }
        
        if let lValue:Int = self.query(select: lSql, types: [.int])?.cast() {
            lRet = lValue
        }
        
        return lRet
    }
    
    public func create(table pTable: String, columns pColumns: [String], types pTypes: [TSql.Field], options pOptions: Options?) throws {
        error = .missing(.database)
        
        let lDataSet = TSqliteDataSet(table: pTable, columns: pColumns, types: pTypes)
        try create(dataSet: lDataSet, options: pOptions)
    }
    
    public func create(dataSet pDataSet: TDataSet, options pOptions: Options?) throws {
        guard db != nil else {
            error = .missing(.database)
            throw error
        }
        
        guard let lTable = pDataSet.table else {
            error = .missing(.table)
            throw error
        }
        
        guard pDataSet.types.count != 0 else {
            error = .missing(.size)
            throw error
        }
        
        let lColumns = pDataSet.columns.count
        
        guard lColumns > 0 else {
            error = .invalid(.columnCount)
            throw error
        }
        
        let lSql = TSqliteBuilder()
            .create(.table, name: lTable)
            .ifNotExist
        
        error = .success
        
        for lI in 0..<lColumns {
            let lColumn = pDataSet.columns[lI]
            let lType = pDataSet.types[lI]
            var lOptions = pOptions?(lColumn) ?? []
            
            if TSql.Column.Option.contains(options: lOptions, subSet: [.toOne,.toMany]) {
                lOptions = TSql.Column.Option.subtracting(options: lOptions, sub: [.not,.null])
            }
            
            lSql.column(name: lColumn, type: lType, options: lOptions)
        }
        
        
        if error == .success {
            try exec(lSql)
        } else {
            throw error
        }
    }
    
    public func create(dataSet pDataSet: TDataSet) throws {
        try create(dataSet: pDataSet, options: nil)
    }
    
    public func insert(dataSet pDataSet:TDataSet?, replace pReplace:Bool) throws {
        guard db != nil else {
            error = .missing(.database)
            throw error
        }
        
        guard let lDataSet = pDataSet else {
            error = .missing(.dataset)
            throw error
        }
        
        guard let lTable = lDataSet.table else {
            error = .missing(.table)
            throw error
        }
        
        let lColumns = lDataSet.columns.count
        
        guard lColumns > 0 else {
            error = .invalid(.columnCount)
            throw error
        }
        
        let lRows = lDataSet.rows
        
        guard lRows.count > 0 else {
            error = .missing(.row)
            throw error
        }
        
        let lSql = TSqliteBuilder()
        
        if pReplace {
            lSql.insertOrReplace(lTable)
        } else {
            lSql.insert(lTable)
        }
        
        lSql.columns(lDataSet.columns)
            .values(lDataSet.columns.map({TSql.Value.var($0)}))
        
        try exec(lSql, lRows)
    }
}
