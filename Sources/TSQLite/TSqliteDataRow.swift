//
//  TSqliteDataRow.swift
//  TSqlite
//
//  Created by Christophe Braud on 20/08/2017.
//  Base on Tof Templates (https://bit.ly/2Zk06Yg)
//  Copyright © 2017 Christophe Braud. All rights reserved.
//

import Foundation
import TDataBase

// MARK: -
// MARK: TSqliteDataRow
// MARK: -
public final class TSqliteDataRow: TDataRow {
  // MARK: -
  // MARK: Public access
  // MARK: -
  
  // MARK: -> Public enums
  
  // MARK: -> Public structs
  
  // MARK: -> Public class
  
  // MARK: -> Public type alias
  
  // MARK: -> Public static properties
  
  // MARK: -> Public properties
  
  public var dataSet: TDataSet? = nil
  public var values: [Any?] = []
  
  // MARK: -> Public class methods
  
  // MARK: -> Public init methods
    public init() {}
    
    public init(row pRow: TDataRow) {
        if let lDataSet = pRow.dataSet {
            dataSet = TSqliteDataSet(dataSet: lDataSet)
        }
        values = pRow.values.map({$0})
    }
    
    public init(dataSet pDataSet: TDataSet) {
        dataSet = pDataSet
    }
    
    public init(dataSet pDataSet: TDataSet, values pValues: [Any?]) {
        dataSet = pDataSet
        
        if pDataSet.columns.count == pValues.count {
            values = pValues.map({$0})
        }
    }
    
    public init(dataSet pDataSet: TDataSet, keys pKeys: [String], values pValues: [Any?]) {
        dataSet = pDataSet

        if pKeys.count == pValues.count && pKeys.count >= pDataSet.columns.count {
            for lColumn in pDataSet.columns {
                if let lIndex = pKeys.firstIndex(where: {$0 == lColumn}), let lValue = pValues[lIndex] {
                    values.append(lValue)
                } else {
                    values.append(nil)
                }
            }
        }
    }
    
  // MARK: -> Public operators
  
  // MARK: -> Public methods
  
  // MARK: -
  // MARK: Internal access (aka public for current module)
  // MARK: -
  
  // MARK: -> Internal enums
  
  // MARK: -> Internal structs
  
  // MARK: -> Internal class
  
  // MARK: -> Internal type alias
  
  // MARK: -> Internal static properties
  
  // MARK: -> Internal properties
  
  // MARK: -> Internal class methods
  
  // MARK: -> Internal init methods
  
  // MARK: -> Internal operators
  
  // MARK: -> Internal methods
  
  // MARK: -
  // MARK: File Private access
  // MARK: -
  
  // MARK: -> File Private enums
  
  // MARK: -> File Private structs
  
  // MARK: -> File Private class
  
  // MARK: -> File Private type alias
  
  // MARK: -> File Private static properties
  
  // MARK: -> File Private properties
  
  // MARK: -> File Private class methods
  
  // MARK: -> File Private init methods
  
  // MARK: -> File Private operators
  
  // MARK: -> File Private methods
  
  // MARK: -
  // MARK: Private access
  // MARK: -
  
  // MARK: -> Private enums
  
  // MARK: -> Private structs
  
  // MARK: -> Private class
  
  // MARK: -> Private type alias
  
  // MARK: -> Private static properties
  
  // MARK: -> Private properties
  
  // MARK: -> Private class methods
  
  // MARK: -> Private init methods
  
  // MARK: -> Private operators
  
  // MARK: -> Private methods
  
}
