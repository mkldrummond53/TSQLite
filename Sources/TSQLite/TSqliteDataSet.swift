//
//  TSqliteDataSet.swift
//  TSqlite
//
//  Created by Christophe Braud on 20/08/2017.
//  Base on Tof Templates (https://bit.ly/2Zk06Yg)
//  Copyright © 2017 Christophe Braud. All rights reserved.
//

import Foundation
import TDataBase

// MARK: -
// MARK: TSqliteDataSet
// MARK: -
public final class TSqliteDataSet: TDataSet {
    // MARK: -
    // MARK: Public access
    // MARK: -
    
    // MARK: -> Public enums
    
    // MARK: -> Public structs
    
    // MARK: -> Public class
    
    // MARK: -> Public type alias
    
    // MARK: -> Public static properties
    
    // MARK: -> Public properties
    
    public var table: String? = nil
    public var columns: [String] = []
    public var types: [TSql.Field] = []
    public var rows: [TDataRow] = []
    
    // MARK: -> Public class methods
        
    // MARK: -> Public init methods
    
    public init() {}
    
    public init(rows pRows: [TDataRow]) {
        if let lDataSet = pRows.first?.dataSet {
            table = lDataSet.table
            columns = lDataSet.columns.map({$0})
            types = lDataSet.types.map({$0})
            rows(pRows)
        }
    }
    
    public init(dataSet pDataSet: TDataSet) {
        table = pDataSet.table
        columns = pDataSet.columns.map({$0})
        types = pDataSet.types.map({$0})
    }
    
    public init(columns pColumns: [String], types pTypes: [TSql.Field]) {
        precondition(pColumns.count == pTypes.count, "number of columns and types must be the same ")
        columns = pColumns.map({$0})
        types = pTypes.map({$0})
    }
    
    public init(columns pColumns: [String], types pTypes: [TSql.Field], rows pRows: [[Any?]]) {
        precondition(pColumns.count == pTypes.count, "number of columns and types must be the same ")
        columns = pColumns.map({$0})
        types = pTypes.map({$0})
        rows(pRows)
    }
    
    public convenience init(table pTable: String, columns pColumns: [String], types pTypes: [TSql.Field]) {
        self.init(columns: pColumns, types: pTypes)
        table = pTable
    }
    
    public convenience init(table pTable: String, columns pColumns: [String], types pTypes: [TSql.Field], rows pRows: [[Any?]]) {
        self.init(columns: pColumns, types: pTypes, rows: pRows)
        table = pTable
    }
    
    // MARK: -> Public operators
    
    // MARK: -> Public methods
    
    public func table(_ pTable: String) {
        table = pTable
    }
    
    public func columns(_ pColumns: [String]) {
        columns = pColumns
    }
    
    public func types(_ pTypes: [TSql.Field]) {
        types = pTypes
    }
    
    public func rows(_ pRows: [TDataRow]) {
        for lRow in pRows {
            rows.append(TSqliteDataRow(dataSet: self, values: lRow.values))
        }
    }
    
    public func rows(_ pRows: [[Any?]]) {
        rows = []
        
        for lRow in pRows {
            rows.append(TSqliteDataRow(dataSet: self, values: lRow))
        }
    }
    
    public func column(_ pIdx: Int) -> String? {
        var lRet:String? = nil
        
        if pIdx >= 0 && pIdx < columns.count {
            lRet = columns[pIdx]
        }
        
        return lRet
    }
    
    public func type(_ pIdx: Int) -> Any.Type? {
        var lRet:Any.Type? = nil
        
        if pIdx >= 0 && pIdx < types.count {
            lRet = types[pIdx].asAnyType
        }
        
        return lRet
    }
    
    public func type(_ pName: String) -> Any.Type? {
        var lRet:Any.Type? = nil
        
        if let lIndex = columns.firstIndex(of: pName) {
            lRet = types[lIndex].asAnyType
        }
        
        return lRet
    }
    
    public func row(_ pIdx: Int) -> TDataRow? {
        var lRet:TDataRow? = nil
        
        if pIdx >= 0 && pIdx < rows.count {
            lRet = rows[pIdx]
        }
        
        return lRet
    }
    
    // MARK: -
    // MARK: Internal access (aka public for current module)
    // MARK: -
    
    // MARK: -> Internal enums
    
    // MARK: -> Internal structs
    
    // MARK: -> Internal class
    
    // MARK: -> Internal type alias
    
    // MARK: -> Internal static properties
    
    // MARK: -> Internal properties
    
    // MARK: -> Internal class methods
    
    // MARK: -> Internal init methods
    
    // MARK: -> Internal operators
    
    // MARK: -> Internal methods
    
    // MARK: -
    // MARK: File Private access
    // MARK: -
    
    // MARK: -> File Private enums
    
    // MARK: -> File Private structs
    
    // MARK: -> File Private class
    
    // MARK: -> File Private type alias
    
    // MARK: -> File Private static properties
    
    // MARK: -> File Private properties
    
    // MARK: -> File Private class methods
    
    // MARK: -> File Private init methods
    
    // MARK: -> File Private operators
    
    // MARK: -> File Private methods
    
    // MARK: -
    // MARK: Private access
    // MARK: -
    
    // MARK: -> Private enums
    
    // MARK: -> Private structs
    
    // MARK: -> Private class
    
    // MARK: -> Private type alias
    
    // MARK: -> Private static properties
    
    // MARK: -> Private properties
    
    // MARK: -> Private class methods
    
    // MARK: -> Private init methods
    
    // MARK: -> Private operators
    
    // MARK: -> Private methods
    
}
